pipeline {
    agent any
    environment {
            SECRET_KEY = credentials('django_secret_key')
            PROJECT_NAME = 'django_ci'
            SERVER_IP = credentials('django_server_ip')
            DEPLOY="export SECRET_KEY=${SECRET_KEY} && \
            cd /var/django/${PROJECT_NAME} && \
            python3 -m venv django_ci && \
            source django_ci/bin/activate && \
            git checkout ${env.GIT_BRANCH} && \
            git pull ${env.GIT_BRANCH.split("/")[0]} ${env.GIT_BRANCH.split("/")[1]} && \
            service uwsgi force-reload"
    }
    stages {
       stage('build') {
          steps {
             updateGitlabCommitStatus name: 'build', state: 'pending'
             script {
               try {
                 sh 'python3 -m venv /var/lib/jenkins/venv/django_ci'
                 sh '. /var/lib/jenkins/venv/django_ci/bin/activate'
                 sh 'pip3 install -r requirements.txt'
                 sh 'python3 manage.py makemigrations'
                 sh 'python3 manage.py migrate'
                 sh 'python3 manage.py collectstatic --noinput'
                 updateGitlabCommitStatus name: 'build', state: 'success'
               } catch (Exception e) {
                 echo 'Exception occurred: ' + e.toString()
                 updateGitlabCommitStatus name: 'build', state: 'failed'
                 sh 'exit 1'
               }
             }
          }
       }
       stage(test) {
           steps {
               updateGitlabCommitStatus name: 'test', state: 'pending'
               script {
                 try {
                   sh 'python3 manage.py test'
                   updateGitlabCommitStatus name: 'test', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'test', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
       }
       stage(linter) {
          steps {
              updateGitlabCommitStatus name: 'linter', state: 'pending'
              script {
                try {
                  sh 'pycodestyle --max-line 120 .'
                  updateGitlabCommitStatus name: 'linter', state: 'success'
                } catch (Exception e) {
                  echo 'Exception occurred: ' + e.toString()
                  updateGitlabCommitStatus name: 'linter', state: 'failed'
                  sh 'exit 1'
                }
              }
          }
       }
       stage(security) {
           steps {
               updateGitlabCommitStatus name: 'security', state: 'pending'
               script {
                 try {
                   sh 'safety check'
                   sh 'python3 manage.py check --deploy'
                   updateGitlabCommitStatus name: 'security', state: 'success'
                 } catch (Exception e) {
                   echo 'Exception occurred: ' + e.toString()
                   updateGitlabCommitStatus name: 'security', state: 'failed'
                   sh 'exit 1'
                 }
               }
           }
        }
        stage(seleniumsiderunner) {
            steps {
                updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'pending'
                script {
                  try {
                    wrap([
                      $class: 'Xvfb', additionalOptions: '', assignedLabels: '',
                              autoDisplayName: true, debug: true, displayNameOffset: 0,
                              installationName: 'xvfb', parallelBuild: true, screen: '1024x758x24', timeout: 30]
                    ) {
                      sh 'selenium-side-runner selenium/*.side -c "goog:chromeOptions.args=[--headless,--nogpu] browserName=chrome" --output-directory=results --output-format=junit'
                    }
                    junit 'results/*xml'
                    updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'success'
                  } catch (Exception e) {
                    echo 'Exception occurred: ' + e.toString()
                    updateGitlabCommitStatus name: 'seleniumsiderunner', state: 'failed'
                    sh 'exit 1'
                  }
                }
            }
        }
        stage(testng) {
            steps {
                updateGitlabCommitStatus name: 'testng', state: 'pending'
                script {
                  try {
                    wrap([
                      $class: 'Xvfb', additionalOptions: '', assignedLabels: '',
                              autoDisplayName: true, debug: true, displayNameOffset: 0,
                              installationName: 'xvfb', parallelBuild: true, screen: '1024x758x24', timeout: 30]
                    ) {
                      withMaven(maven: 'maventheroot') {
                            sh "mvn clean test"
                      }
                    }
                    step([$class: 'Publisher', reportFilenamePattern: '**/testng-results.xml'])
                    updateGitlabCommitStatus name: 'testng', state: 'success'
                  } catch (Exception e) {
                    echo 'Exception occurred: ' + e.toString()
                    updateGitlabCommitStatus name: 'testng', state: 'failed'
                    sh 'exit 1'
                  }
                }
            }
        }
       stage(deploy) {
          steps {
              updateGitlabCommitStatus name: 'deploy', state: 'pending'
              script {
                try {
                  sh '''
                  ssh-keyscan ${SERVER_IP} >> ~/.ssh/known_hosts &&
                  ssh -A root@${SERVER_IP} "$DEPLOY"
                  '''
                  updateGitlabCommitStatus name: 'deploy', state: 'success'
                } catch (Exception e) {
                  echo 'Exception occurred: ' + e.toString()
                  updateGitlabCommitStatus name: 'deploy', state: 'failed'
                  sh 'exit 1'
                }
              }
          }
       }
    }
 }
